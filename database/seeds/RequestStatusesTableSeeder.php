<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('request_statuses')->insert([
        	'name'=> 'approved'
        ]);

        DB::table('request_statuses')->insert([
        	'name'=> 'declined'
        ]);

        DB::table('request_statuses')->insert([
        	'name'=> 'completed'
        ]);
    }
}
