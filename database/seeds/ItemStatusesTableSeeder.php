<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ItemStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_statuses')->insert([
        	'name'=> 'available'
        ]);

        DB::table('item_statuses')->insert([
        	'name'=> 'out'
        ]);

        DB::table('item_statuses')->insert([
        	'name'=> 'under maintenance'
        ]);

        DB::table('item_statuses')->insert([
        	'name'=> 'out of service'
        ]);
    }
}
