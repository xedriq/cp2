<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->string('name');
            $table->string('brand_name')->nullable(true);
            $table->string('model_name')->nullable(true);
            $table->string('serial_number')->nullable(true);
            $table->text('description')->nullable(true);
            $table->string('image')->nullable(true)->default(null);

            $table->unsignedBigInteger('item_status_id')->default(1); // fk from statuses table
            $table->foreign('item_status_id')
                ->references('id')
                ->on('item_statuses')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('category_id')->nullable(true); // fr from categories table
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
