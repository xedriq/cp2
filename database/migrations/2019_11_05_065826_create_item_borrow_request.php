<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemBorrowRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_borrow_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            
            //fk item_id
            $table->unsignedBigInteger('item_id');
            $table->foreign('item_id')
                ->references('id')->on('items')
                ->onDelete('restrict')
                ->onUpdate('cascade');

             //fk borrow_request_id
            $table->unsignedBigInteger('borrow_request_id')->nullable(true);
            $table->foreign('borrow_request_id')
                ->references('id')->on('borrow_requests')
                ->onDelete('set null')
                ->onUpdate('cascade');

            // is returned?
            $table->boolean('is_return')->default(true);
            // start date
            $table->dateTime('start_date');
            // return date
            $table->dateTime('return_date');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('borrow_requests');
    }
}
