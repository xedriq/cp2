<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrow_request extends Model
{
    protected $fillable = ['request_status_id'];

    public function request_status(){
        return $this->belongsTo('App\Request_status');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }


    public function items(){
        return $this->belongsToMany('App\Item','item_borrow_request')
            ->withPivot('is_return','start_date', 'return_date')
            ->withTimestamps();
    }
}
