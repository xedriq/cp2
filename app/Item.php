<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'brand_name',
        'model_name',
        'category_id',
        'serial_number',
        'image',
        'item_status_id',
        'description'
    ];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function item_status(){
        return $this->belongsTo('App\Item_status');
    }

    public function borrow_requests(){
        return $this->belongsToMany('App\Borrow_request','item_borrow_request')
            ->withPivot('is_return','start_date', 'return_date')
            ->withTimestamps();
    }


}
