<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request_status extends Model
{
    public function borrow_requests(){
    	return $this->hasMany('App\Borrow_request');
    }
}
