<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Borrow_request;

class BorrowRequestProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        
        View::composer('layouts.app', function($view){
            $view->with('pending_requests', Borrow_request::all()->where('request_status_id', null));
        });
    }
}
