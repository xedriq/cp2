<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Item;
use App\Borrow_request;
use App\Policies\ItemPolicy;
use App\Policies\Borrow_requestPolicy;
use Auth;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Item::class => ItemPolicy::class,
        Borrow_request::class => Borrow_requestPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::define('isAdmin', function($user){
            return $user->role_id == 1;
        });

        Gate::define('isLogged', function($user){
            return Auth::user();
        });        
    }
}
