<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_status extends Model
{
    public function items(){
    	return $this->hasMany('App\Item');
    }
}
