<?php

namespace App\Http\Controllers;

use App\Item;
use App\Item_status;
use App\Category;
use Illuminate\Http\Request;
use Str;
use Storage;
use Auth;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('isLogged');
        // dd($request->url());
        if(Auth::user() && Auth::user()->role_id === 1) {
            if($request->query()){
                $items = Item::where('category_id', $request->query('category'))->get();
            } else {
                $items = Item::all();
            }
            return view('items.index')->with('items', $items)->with('categories', Category::all());
        } else {
            return view('items.index')->with('items', Item::all()->where('item_status_id', 1));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin');

        return view('items.create')
            ->with('item_statuses', Item_status::all())
            ->with('categories', Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');

        //validation
        $request->validate([
            'brand-name' => 'required|string',
            'model-name' => 'required|string',
            'category' => 'string',
            'serial-number'=>'required|unique:items,serial_number',
            'image'=> 'file|image|mimes:jpeg,jpg,png|max:2000',
            'item-status' => 'string',
            'description' => 'string'
        ]);

        $file_path=''; //set default image

        if($request->hasFile('image')){
            // get image path before saving
            $file = $request->file('image');
            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_extenstion = $file->extension();
            $random_char = Str::random();
            $new_file_name = date('Y-m-d-H-i-s') . '_' . $random_char . '_' . $file_name . '.' . $file_extenstion;
            $file_path = $file -> storeAs('images', $new_file_name, 'public');
        };

        // save to db
        Item::create([
            'brand_name' => $request->input('brand-name'),
            'model_name' => $request->input('model-name'),
            'category_id' => $request->input('category'),
            'serial_number' => $request->input('serial-number'), 
            'item_status_id' => $request->input('item-status'),
            'description' => $request->input('description'),
            'image' => $file_path
        ]);

        $request->session()->flash('success', 'Item added successfully.');

        return redirect(route('items.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
        return view('items.show')->with('item', $item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $this->authorize('isAdmin');

        // dd($item);
        return view('items.create')
            ->with('item', $item)
            ->with('categories', Category::all())
            ->with('item_statuses', Item_status::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $this->authorize('isAdmin');

         //validation
        $request->validate([
            'brand-name' => 'required|string',
            'model-name' => 'required|string',
            'category' => 'string',
            'serial-number'=>'required',
            'item-status' => 'string',
            'description' => 'required',
            'image'=> 'file|image|mimes:jpeg,jpg,png|max:2000'

        ]);


        if(
            !$request->hasFile('image') &&
            $item->brand_name == $request->input('brand-name') &&
            $item->model_name == $request->input('model-name') &&
            $item->description == $request->input('description') &&
            $item->category_id == $request->input('category') &&
            $item->item_status_id == $request->input('item-status') &&
            $item->serial_number == $request->input('serial-number')
        ){

            $request->session()->flash('info', 'No changes has been made.');

            return redirect(route('items.edit', ['item'=>$item->id]));
        } else {

            // update the entry in the database and return the updated entry
            
            // check if there's a file uploaded
            if ($request->hasFile('image')) {
                // $request->validate([
                    //     'image'=> 'file|image|mimes:jpeg,jpg,png|max:2000'
                    // ]);

                $file = $request->file('image');
                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file_extenstion = $file->extension();
                $random_char = Str::random();
                $new_file_name = date('Y-m-d-H-i-s') . '_' . $random_char . '_' . $file_name . '.' . $file_extenstion;
                $file_path = $file -> storeAs('images', $new_file_name, 'public');

                // set the new image path as image value of the product
                $item->image = $file_path;

            }
            // save to db
            $item->update([
            'brand_name' => $request->input('brand-name'),
            'model_name' => $request->input('model-name'),
            'category_id' => $request->input('category'),
            'serial_number' => $request->input('serial-number'), 
            'item_status_id' => $request->input('item-status'),
            'description' => $request->input('description')
            ]);

            $request->session()->flash('success', 'Item updated successfully.');

        return redirect(route('items.edit', ['item'=>$item->id]));

        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {

        $this->authorize('isAdmin');

        $item->delete();
        Storage::delete('public/'.$item->image);

        return redirect(route('items.index'))->with('info', 'Item deleted successfully.');

    }
}
