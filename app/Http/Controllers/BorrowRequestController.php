<?php

namespace App\Http\Controllers;

use App\Borrow_request;
use Illuminate\Http\Request;
use Auth;
use Str;
use Session;
use App\Item;

class BorrowRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isLogged');
        if(Auth::user()->role_id === 1){

            $pending_requests = Borrow_request::all()->where('request_status_id', null);
            $approved_requests = Borrow_request::all()->where('request_status_id', 1);
            $declined_requests = Borrow_request::all()->where('request_status_id', 2);
            $completed_requests = Borrow_request::all()->where('request_status_id', 3);

        } else {
            $pending_requests = Borrow_request::all()->where('request_status_id', null)->where('user_id', Auth::user()->id);
            $approved_requests = Borrow_request::all()->where('request_status_id', 1)->where('user_id', Auth::user()->id);
            $declined_requests = Borrow_request::all()->where('request_status_id', 2)->where('user_id', Auth::user()->id);
            $completed_requests = Borrow_request::all()->where('request_status_id', 3)->where('user_id', Auth::user()->id);            
        }
        return view('borrow_requests.index')
            ->with('pending_requests', $pending_requests)
            ->with('declined_requests', $declined_requests)
            ->with('completed_requests', $completed_requests)
            ->with('approved_requests', $approved_requests);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'borrow-date' => 'date',
            'return-date' => 'date',
        ]);
        //instantiate $borrow_request
        $borrow_request = new Borrow_request;

        // prepare data
        $borrow_request_number = Auth::user()->id . '_' . Str::random(10) .'_'. time();
        $user_id = Auth::user()->id;

        // set and save data to borrow_request table
        $borrow_request->borrow_request_number = $borrow_request_number;
        $borrow_request->user_id = $user_id;
        $borrow_request->save();

        // get item id on session
        $item_ids = array_keys(Session::get('crate'));

        // fetch items from db using ids from session
        $items = Item::find($item_ids);

        // dd(Session::get('crate'));

        foreach (Session::get('crate') as $crate_item_id => $dates) {
            foreach ($items as $item) {
                if($item->id == $crate_item_id){
                    $borrow_request->items()->attach(
                        $item->id, [
                            'start_date'=>$dates['borrow-date'],
                            'return_date'=>$dates['return-date']
                        ]
                    );
                }
            }
        }

        Session::forget('crate');

        return redirect(route('borrow_requests.show', ['borrow_request' => $borrow_request->id]));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Borrow_request  $borrow_request
     * @return \Illuminate\Http\Response
     */
    public function show(Borrow_request $borrow_request)
    {
        //
        $this->authorize('view', $borrow_request);

        return view('borrow_requests.show')
            ->with('borrow_request', $borrow_request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Borrow_request  $borrow_request
     * @return \Illuminate\Http\Response
     */
    public function edit(Borrow_request $borrow_request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Borrow_request  $borrow_request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Borrow_request $borrow_request)
    {
        // dd(Session::get('transaction_items'));
        // dd($request->input('items'));
        // $request->validate([
        //     'request-action' => 'required',
        //     'items'=>'required'
        // ]);
        if($request->session()->has('transaction_items')){
            $item_ids = array_values(Session::get('transaction_items'));
            $items = Item::find($item_ids);

            $borrow_request->update([
                'request_status_id' => $request->input('request-action')
            ]);
        }


        // $items = $request->input('items');
        // dd($items);
        switch($request->input('request-action')){
            case 1: 
                $request->session()->flash('info', 'A request has been approved.');
                
                foreach ($items as $item) {
                    $item->update([
                        'item_status_id' => 2
                    ]);
                }

                Session::forget('transaction_items');
                break;
            case 2:
                $request->session()->flash('info', 'A request has been declined.');
                Session::forget('transaction_items');
                break;
            case 3:
                $request->session()->flash('info', 'A request has been completed.');
                foreach ($items as $item) {
                    $item->update([
                        'item_status_id' => 1
                    ]);
                }
                Session::forget('transaction_items');
                break;
        }

        return redirect(route('borrow_requests.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Borrow_request  $borrow_request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Borrow_request $borrow_request)
    {
        //
        // dd($borrow_request);
        $borrow_request->delete();

        return redirect(route('borrow_requests.index'))->with('info', 'Borrow request cancelled.');
    }
}
