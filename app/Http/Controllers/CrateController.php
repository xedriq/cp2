<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use Session;

class CrateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isLogged');

        if(Session::has('crate')){
            $item_ids = array_keys(Session::get('crate'));

            //fetch items
            $items = Item::find($item_ids);
            
            return view('crates.index')->with('items', $items);
        }

        return view('crates.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('isLogged');

        //
        // dd($request->session()->all());
        // dd($request->input('id'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isLogged');

        $request->validate([
            'id'=>'required',
            'borrow-date'=>'date',
            'return-date'=>'date',
        ]);

        // crate format

        // borrow request id
        // item id => borrow date
        // item_id => return date

        // $cart = [
        //     id => [
        //         borrow-date => date,
        //         return-date => date
        //     ]
        // ]
        // $item = Item::find($id);

        $id = $request->input('id');
        $borrow_date = $request->input('borrow-date');
        $return_date = $request->input('return-date');

        $request->session()->put("crate.$id", [
                'borrow-date' => $borrow_date,
                'return-date' => $return_date
            ]);

        return redirect(route('crates.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $this->authorize('isLogged');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->authorize('isLogged');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->authorize('isLogged');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->authorize('isLogged');
        
        // dd($id);
        Session::forget("crate.$id");

        return redirect(route('crates.index'));
    }

    public function clear_crate(){

        Session::forget('crate');
        
        return redirect(route('crates.index'))->with('info', 'Crate is cleared.');
    }
}
