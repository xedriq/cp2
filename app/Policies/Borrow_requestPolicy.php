<?php

namespace App\Policies;

use App\Borrow_request;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class Borrow_requestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any borrow_requests.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the borrow_request.
     *
     * @param  \App\User  $user
     * @param  \App\Borrow_request  $borrowRequest
     * @return mixed
     */
    public function view(User $user, Borrow_request $borrowRequest)
    {
        //
        return $user->id === $borrowRequest->user_id || $user->role_id === 1;
    }

    /**
     * Determine whether the user can create borrow_requests.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the borrow_request.
     *
     * @param  \App\User  $user
     * @param  \App\Borrow_request  $borrowRequest
     * @return mixed
     */
    public function update(User $user, Borrow_request $borrowRequest)
    {
        //
    }

    /**
     * Determine whether the user can delete the borrow_request.
     *
     * @param  \App\User  $user
     * @param  \App\Borrow_request  $borrowRequest
     * @return mixed
     */
    public function delete(User $user, Borrow_request $borrowRequest)
    {
        //
    }

    /**
     * Determine whether the user can restore the borrow_request.
     *
     * @param  \App\User  $user
     * @param  \App\Borrow_request  $borrowRequest
     * @return mixed
     */
    public function restore(User $user, Borrow_request $borrowRequest)
    {
        //
        return $user->role_id === 1;
    }

    /**
     * Determine whether the user can permanently delete the borrow_request.
     *
     * @param  \App\User  $user
     * @param  \App\Borrow_request  $borrowRequest
     * @return mixed
     */
    public function forceDelete(User $user, Borrow_request $borrowRequest)
    {
        //
        return $user->role_id === 1;
    }
}
