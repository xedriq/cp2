@extends('layouts.app')

@section('content')
{{-- {{ dd(Session::all()) }} --}}
<h2 class="text-center mb-3">Add to Crate</h2>
<div class="row">
	<div class="col-6 text-center">
		<img src="{{ url('public/'.$item->image) }}" class="img-thumbnail w-25 mb-3">
		<h4>{{ $item->brand_name }} {{$item->model_name}}</h4>
		<p>{{ $item->description }}</p>
		<p>Serial #: {{ $item->serial_number }}</p>
		<p>Status: {{ $item->item_status_id ? $item->item_status->name : 'none' }}</p>
	</div>

	<div class="col-6">
		<form action="{{ route('crates.store') }}" method="POST">
			@csrf
			<input type="hidden" name="id" value="{{ $item->id }}">
			<div class="form-group">
				<label for="borrow-date">Borrow Date and Time:</label>
				<input type="borrow-date" name="borrow-date" id="borrow-date" class="form-control
				@if($errors->has('borrow-date'))
                is-invalid
                @endif
				">
				<div class="invalid-feedback">{{ $errors->first('borrow-date') }}</div>
			</div>

			<div class="form-group">
				<label for="return-date">Return Date and Time:</label>
				<input type="return-date" name="return-date" class="form-control
				@if($errors->has('return-date'))
                is-invalid
                @endif
				" id="return-date">
				<div class="invalid-feedback">{{ $errors->first('return-date') }}</div>
			</div>
			<button class="btn btn-success">Add to Crate</button>
		</form>
	</div>
</div>


@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
	<script>
		flatpickr('#borrow-date', {
		    enableTime: true,
		    dateFormat: "Y-m-d H:i",
		})

		flatpickr('#return-date', {
		    enableTime: true,
		    dateFormat: "Y-m-d H:i",
		})
	</script>
@endsection

@section('css')
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
