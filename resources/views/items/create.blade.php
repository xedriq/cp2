@extends('layouts.app')

@section('content')
<h2 class="text-center">{{ isset($item) ? 'Edit Item' : 'Add Items' }}</h2>
<div class="row">
    <div class="col-12 col-md-8 mx-auto">
        <form action="{{ isset($item) ?  route('items.update', $item->id) : route('items.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            @if(isset($item))
                @method('PUT')
            @endif

            <div class="form-group">
                <label for="brand-name">Brand Name:</label>
                <input type="text" name="brand-name" id="brand-name" class="form-control
                @if($errors->has('brand-name'))
                is-invalid
                @endif
                " value="{{ isset($item) ? $item->brand_name : ''}}">
                <div class="invalid-feedback">{{ $errors->first('brand-name') }}</div>
            </div>
            
            <div class="form-group">
                <label for="model-name">Model Name:</label>
                <input type="text" name="model-name" id="model-name" class="form-control
                @if($errors->has('model-name'))
                is-invalid
                @endif
                " value="{{ isset($item) ? $item->model_name : ''}}">
                <div class="invalid-feedback">{{ $errors->first('model-name') }}</div>
            </div>

            <div class="form-group">
                <label for="category">Category:</label>
                <select class="custom-select
                @if($errors->has('category'))
                is-invalid
                @endif
                " name="category" id="category">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}"
                            @if(isset($item))
                            @if($item->category_id === $category->id)
                                selected 
                            @endif
                            @endif
                        >{{ $category->name }}</option>
                    @endforeach
                </select>
                <div class="invalid-feedback">{{ $errors->first('category') }}</div>
            </div>
            
            <div class="form-group">
                <label for="serial-number">Serial #:</label>
                <input type="text" name="serial-number" id="serial-number" class="form-control
                @if($errors->has('serial-number'))
                is-invalid
                @endif
                " value="{{ isset($item) ? $item->serial_number : ''}}"
                >
                <div class="invalid-feedback">{{ $errors->first('serial-number') }}</div>
            </div>
            
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea name="description" id="description" cols="10" rows="5" class="form-control
                @if($errors->has('description'))
                is-invalid
                @endif
                ">{{ isset($item) ? $item->description : '' }}</textarea>
                <div class="invalid-feedback">{{ $errors->first('description') }}</div>
            </div>

            @if(isset($item))
            <div>
                <img src="{{ asset('public/'.$item->image) }}" class="img-thumbnail" style="max-width:300px">
            </div>
            @endif

            <div class="form-group">
                <div class="custom-file">
                    <label for="image">Image:</label>
                    <input type="file" name="image" id="image" class="
                    @if($errors->has('image'))
                    is-invalid
                    @endif
                    ">
                    <div class="invalid-feedback">{{ $errors->first('image') }}</div>
                </div>
            </div>
            
            <div class="form-group">
                <label for="item-status">Status:</label>
                <select class="custom-select
                @if($errors->has('item-status'))
                is-invalid
                @endif
                " name="item-status" id="item-status">
                    @foreach ($item_statuses as $item_status)
                        <option value="{{ $item_status->id  }}"
                            @if(isset($item))
                                @if($item->item_status_id === $item_status->id)
                                    selected 
                                @endif
                            @endif
                        >{{ $item_status->name }}</option>
                    @endforeach
                    
                </select>
                <div class="invalid-feedback">{{ $errors->first('item-status') }}</div>
            </div>

            <button class="btn btn-success float-right">{{ isset($item) ? 'Update Item' : 'Add Item' }}</button>
        </form>
        @if(isset($item))
        <form action="{{ route('items.destroy', $item->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger float-right mx-3">Delete Item</button>
        </form>
        @endif
    </div>
</div>

@endsection