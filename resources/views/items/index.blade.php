@extends('layouts.app')

@section('content')

<h2 class="text-center">Equipment List</h2>
@can('isAdmin')
<div class="row">
    <div class="col-10 d-flex justify-content-end">
        <ul class="nav">
            <li class="nav-item dropdown show">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Category Filter</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 35px, 0px);">
                    @foreach($categories as $category)
                        <a class="dropdown-item" href="{{ route('items.index', ['category' => $category->id]) }}">{{$category->name}}</a>
                    @endforeach
                </div>
            </li>
        </ul>
    </div>
    <div class="col-2 d-flex justify-content-end">
        <div>
            <a href="{{ route('items.create') }}" class="btn btn-success ml-3 mb-3">Add Item</a>
        </div>
    </div>
</div>
@endcan

<div class="row">
    <div class="col-12 col-md-12 mx-auto">
        @if($items->count() > 0)
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Photo</th>
                    <th>Brand</th>
                    <th>Model</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Serial #</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($items as $item)
                <tr>
                    <td>
                        <img src="{{ url('public/'.$item->image) }}" class="img-thumbnail" style="max-width:100px">
                    </td>
                    <td>{{ $item->brand_name }}</td>
                    <td>{{ $item->model_name }}</td>
                    <td>{{ $item->category_id ? $item->category->name : 'none'}}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->serial_number }}</td>
                    <td>{{ $item->item_status_id ? $item->item_status->name : 'none' }}</td>
                    <td>
                        @cannot('isAdmin')
                        <a href="{{ route('items.show', $item->id) }}" class="btn btn-primary btn-sm my-3 {{$item->item_status_id != 1 ? 'disabled' : ''}}" >Borrow this</a>
                        @endcannot
                        <form>
                            @csrf
                            @method('PUT')
                            @can('isAdmin')
                            <a href="{{ route('items.edit', $item->id) }}" class="btn btn-info btn-sm">Edit</a>
                            @endcan
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <h3>No items yet.</h3>
        @endif
    </div>
</div>

@endsection