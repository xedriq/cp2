@extends('layouts.app')

@section('content')
{{-- {{ dd(Session::get('crate')) }} --}}
<h3 class="text-center">{{Auth::user() ? Auth::user()->name."'s Crate" : 'Your Crate'}}</h3>
<div class="row">
	<div class="col-12">
		@if(Session::has('crate') && count(Session::get('crate')))
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Name</th>
					<th>Serial #</th>
					<th>Borrow Date and Time</th>
					<th>Return Date and Time</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				{{-- {{dd(Session::get('crate'))}} --}}
				@foreach($items as $item)
				<tr>
					<td>{{ $item->brand_name . ' ' . $item->model_name }}</td>
					<td>{{ $item->serial_number }}</td>
					<td>{{ Session::get("crate.$item->id")["borrow-date"] }}</td>
					<td>{{ Session::get("crate.$item->id")["return-date"] }}</td>
					<td>
						<form action="{{ route('crates.destroy', $item->id) }}" method="POST">
							@csrf
							@method('DELETE')
							<button class="btn btn-danger btn-sm">Remove</button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td>
						<form action="{{ route('borrow_requests.store') }}" method="POST"> 
							@csrf
							<button class="btn btn-success">Finalize Crate</button>
						</form>
					</td>
					<td>
						<form action="{{ route('crates.clear') }}" method="POST">
							@csrf
							@method('DELETE')
							<button class="btn btn-danger" type="submit">Clear Crate</button>
						</form>
					</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tfoot>
		</table>
		@else
			<h3>Your crate is empty.</h3>
		@endif
		
	</div>
</div>

@endsection