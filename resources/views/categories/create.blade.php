@extends('layouts.app')

@section('content')

<div class="container">
    @if(Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif

    <h1 class="text-center">
    {{ isset($category) ? "Edit Category" : "Add Category" }}
    </h1>
    <div class="row">
        <div class="col-12 col-md-12 mx-auto">    
            <form action="{{ isset($category) ? route('categories.update',['category'=>$category->id]) : route('categories.store') }}" method="POST">
                @csrf
                @if(isset($category))
                    @method('PUT')
                @endif
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" class="form-control
                    @if($errors->has('name'))
                    is-invalid
                    @endif
                    " value="{{
                        isset($category) ? $category->name : ''
                    }}">
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                </div>

                <button type="submit" class="btn btn-success">{{ isset($category) ? "Update" : "Add Category" }}</button>
            </form>
        </div>
    </div>
</div>

@endsection