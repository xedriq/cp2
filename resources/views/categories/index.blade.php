@extends('layouts.app')

@section('content')

@if($errors->any())
    dd($errors->first())
@endif

<div class="container">
    <h1 class="text-center">Category List</h1>
        <div class="row">
            <div class="col-12 col-md-8 mx-auto">
            <div class="d-flex justify-content-end mb-3">
                <a class="btn btn-success" href="{{ route('categories.create') }}">Add Category</a>
            </div>

            @if($categories->count()>0)
            <table class="table table-hover">
                <tbody>
                    @foreach($categories as $category )
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td class="text-center"><a href="{{ route('categories.edit',['category'=>$category->id]) }}" class="btn btn-info btn-sm">Edit</a></td>
                        <td class="text-center">
                        <form action="{{ route('categories.destroy',['category'=>$category->id]) }}" method="POST">
                            @csrf
                            @method("DELETE")
                            <button class="btn btn-danger btn-sm">Delete</button>
                        </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <h4>No categories yet.</h4>
            @endif


            </div>
        </div>
</div>

@endsection