@extends('layouts.app')

@section('content')
{{-- {{dd(Auth::user()->id)}} --}}
	<h3 class="text-center">Pending Borrow Requests</h3>
	<div class="row mt-4">
		<div class="col-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Request Code</th>
						<th>Borrower</th>
						<th>Items</th>
						<th>Serial #</th>
						<th>Borrow Date</th>
						<th>Return Date</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($pending_requests as $pending_request)
						<tr>
							<td>{{ $pending_request->borrow_request_number }}</td>
							<td>{{ $pending_request->user->name }}</td>
							<td>
								@foreach ($pending_request->items as $item)
									<p >{{ $item->brand_name.' '.$item->model_name }}</p>
								@endforeach
							</td>
							<td>
								@foreach ($pending_request->items as $item)
								<p >{{ $item->serial_number }}</p>
								@endforeach
							</td>
							<td>
								@foreach ($pending_request->items as $item)
								<p>{{ $item->pivot->start_date}}</p>
								@endforeach
							</td>
							<td>
								@foreach ($pending_request->items as $item)
								<p>{{ $item->pivot->return_date}}</p>
								@endforeach
							</td>
							@can('isAdmin')
							<td class="text-center">
								<form action="{{route('borrow_requests.update', $pending_request->id)}}" method="POST" class="my-3">
									@csrf
									@method('PUT')

									@foreach($pending_request->items as $item)
										{{Session::put("transaction_items.$item->id", $item->id)}}
									@endforeach

									{{-- {{ dd(Session::get('transaction_items'))}} --}}
									{{-- {{dd(Session::all())}} --}}
									{{-- {{dd($pending_request->items->id)}} --}}
									{{-- <input type="hidden" name="items" value="{{ $pending_request->items }}"> --}}
									<input type="hidden" value="1" name="request-action">
									<button class="btn btn-success btn-sm">Approve</button>
								</form>

								<form action="{{route('borrow_requests.update', $pending_request->id)}}" method="POST">
									@csrf
									@method('PUT')
									<input type="hidden" value="2" name="request-action">
									<button class="btn btn-danger btn-sm mx-3">Decline</button>
								</form>

							</td>
							@endcan

							@cannot('isAdmin')
							<td>
								<form action="{{route('borrow_requests.destroy', $pending_request->id)}}" method="POST">
									@csrf
									@method('DELETE')
									{{-- <input type="hidden" value="2" name="request-action"> --}}
									<button class="btn btn-danger btn-sm mx-3">Cancel</button>
								</form>
							</td>
							@endcannot
							
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<h3 class="text-center mt-4">Approved Borrow Requests</h3>
	<div class="row">
		<div class="col-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Request Code</th>
						<th>Borrower</th>
						<th>Items</th>
						<th>Serial #</th>
						
						@can('isAdmin')
						<th>Action</th>
						@endcan
					</tr>
				</thead>
				<tbody>
					@foreach ($approved_requests as $approved_request)
						<tr>
							<td>{{ $approved_request->borrow_request_number }}</td>
							<td>{{$approved_request->user->name}}</td>
							<td>
								@foreach ($approved_request->items as $item)
									<p >{{ $item->brand_name.' '.$item->model_name }}</p>
								@endforeach
							</td>
							<td>
								@foreach ($approved_request->items as $item)
								<p >{{ $item->serial_number }}</p>
								@endforeach
							</td>
							@can('isAdmin')
							<td class="d-flex">
								<form action="{{route('borrow_requests.update', $approved_request->id)}}" method="POST">
									@csrf
									@method('PUT')
									@foreach($approved_request->items as $item)
										{{Session::put("transaction_items.$item->id", $item->id)}}
									@endforeach
									<input type="hidden" value="3" name="request-action">
									<button class="btn btn-success btn-sm">Completed</button>
								</form>
							</td>
							@endcan
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<h3 class="text-center mt-4">Declined Borrow Requests</h3>
	<div class="row">
		<div class="col-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Request Code</th>
						<th>Borrower</th>
						<th>Items</th>
						<th>Serial #</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($declined_requests as $declined_request)
						<tr>
							<td>{{ $declined_request->borrow_request_number }}</td>
							<td>{{$declined_request->user->name}}</td>
							<td>
								@foreach ($declined_request->items as $item)
									<p >{{ $item->brand_name.' '.$item->model_name }}</p>
								@endforeach
							</td>
							<td>
								@foreach ($declined_request->items as $item)
								<p >{{ $item->serial_number }}</p>
								@endforeach
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	
	<h3 class="text-center mt-4">Completed Borrow Requests</h3>
	<div class="row">
		<div class="col-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Request Code</th>
						<th>Borrower</th>
						<th>Items</th>
						<th>Serial #</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($completed_requests as $completed_request)
						<tr>
							<td>{{ $completed_request->borrow_request_number }}</td>
							<td>{{$completed_request->user->name}}</td>
							<td>
								@foreach ($completed_request->items as $item)
									<p >{{ $item->brand_name.' '.$item->model_name }}</p>
								@endforeach
							</td>
							<td>
								@foreach ($completed_request->items as $item)
								<p >{{ $item->serial_number }}</p>
								@endforeach
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

@endsection