@extends('layouts.app')

@section('content')
	<h3 class="text-center mb-4">Borrow Request</h3>
	{{-- {{ dd($borrow_request->items) }} --}}
	
	<div class="row">
		<div class="col-12">
			<h4>Borrowers Name: {{ $borrow_request->user->name }} </h4>
			<table class="table">
				<thead>
					<tr>
						<th>Items</th>
						<th>Borrow Date and Time</th>
						<th>Return Date and Time</th>
						<th>Item Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($borrow_request->items as $item)
					<tr>
						<td>
							{{ $item->brand_name .' '. $item->model_name}}
						</td>
						<td>
							{{ $item->pivot->start_date }}
						</td>
						<td>
							{{ $item->pivot->return_date }}
						</td>
						<td>{{ $item->item_status->name }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection