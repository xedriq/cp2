@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3>Welcome {{ Auth::user()->name }}</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @auth
                       
                    @if(Auth::user()->role_id === 1)
                    You can now start managing our equipments and borrow requests.
                    @else
                    You can now start a request to borrow our available equipments.
                    @endif
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
