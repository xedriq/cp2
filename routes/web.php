<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function(){
    return redirect(route('login'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::delete('crates', 'CrateController@clear_crate')->name('crates.clear');

Route::resource('categories', 'CategoryController');
Route::resource('items', 'ItemController');
Route::resource('crates', 'CrateController');
Route::resource('borrow_requests', 'BorrowRequestController');

